import org.lwjgl.*;
import org.lwjgl.glfw.*;
import org.lwjgl.opengl.*;
import org.lwjgl.system.*;

import java.nio.*;

import static org.lwjgl.glfw.Callbacks.*;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.system.MemoryStack.*;
import static org.lwjgl.system.MemoryUtil.*;
public class Window {

  private final String title;

  // Window dimensions
  private int width;
  private int height;

  // This is used so OpenGL can find the window to populate it
  private long windowHandle;

  private boolean resized;

  // Determines if the game loop should use Vsync
  private boolean vSync;

  /*
   * Constructors
   * */

  //Default Constructor
  public Window() {
    title = "Default title";
    width = 600;
    height = 800;
    vSync = false;
    resized = false;
  }

  //The *USEFULL* Constructor
  public Window (
          String titleInit,
          int widthInit,
          int heightInit,
          boolean vSyncInit) {
    title = titleInit;
    width = widthInit;
    height = heightInit;
    vSync = vSyncInit;
    resized = false;
  }

  public void init() {
    // Tells OpenGL where to send its error messages.
    GLFWErrorCallback.createPrint(System.err).set();

    // Start GLFW and throw an exception otherwise
    if (!glfwInit()) {
      throw new IllegalStateException("GLFW didnt start");
    }

    //NOT 100% what all of these do **NEEDS COMMENTS**
    glfwDefaultWindowHints(); // optional, the current window hints are already the default
    glfwWindowHint(GLFW_VISIBLE, GL_FALSE); // the window will stay hidden after creation
    glfwWindowHint(GLFW_RESIZABLE, GL_TRUE); // the window will be resizable
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

    // Create the window
    windowHandle = glfwCreateWindow(width, height, title, NULL, NULL);
    if (windowHandle == NULL) {
      throw new RuntimeException("Failed to create the GLFW window");
    }


    /*
     * NOTE: since OpenGL and glwf are C libs and lwjgl is basically just a wrapper
     * around them we need to use java lambda to access C's function pointers.
     * */

    // Setup resize callback
    glfwSetFramebufferSizeCallback(windowHandle, (window, width, height) -> {
      this.width = width;
      this.height = height;
      this.setResized(true);
    });

    // Setup a key callback.
    glfwSetKeyCallback(windowHandle, (window, key, scancode, action, mods) -> {
      if (key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE) {
        glfwSetWindowShouldClose(window, true); // We will detect this in the rendering loop
      }
    });

    // Get the resolution of the primary monitor
    GLFWVidMode vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());
    // Center our window
    glfwSetWindowPos(
            windowHandle,
            (vidmode.width() - width) / 2,
            (vidmode.height() - height) / 2
    );

    // Make the OpenGL context current
    glfwMakeContextCurrent(windowHandle);

    if (isvSync()) {
      // Enable v-sync
      glfwSwapInterval(1);
    }

    // Make the window visible
    glfwShowWindow(windowHandle);

    GL.createCapabilities();

    // Set the clear color
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
  }

  public void setClearColor(float r, float g, float b, float alpha) {
    glClearColor(r, g, b, alpha);
  }

  public boolean isKeyPressed(int keyCode){
    return glfwGetKey(windowHandle, keyCode) == GLFW_PRESS;
  }

  public boolean windowShouldClose() {
    return glfwWindowShouldClose(windowHandle);
  }

  /*
   * Getters
   * */

  public String getTitle() {
    return title;
  }

  public int getWidth() {
    return width;
  }

  public int getHeight() {
    return height;
  }

  public boolean isvSync() {
    return vSync;
  }

  public boolean isResized() {
    return resized;
  }

  /*
   * Setters
   * */

  public void setResized(boolean resized) {
    this.resized = resized;
  }

  public void setvSync(boolean vSync) {
    this.vSync = vSync;
  }

  public void update() {
    glfwSwapBuffers(windowHandle);
    glfwPollEvents();
  }
}
