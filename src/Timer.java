/*
* This class is really just a collection of basic
* helper methods that will be used for the main game
* loop.
* */


public class Timer {
  private double lastLoopTime;

  public void init() {
    lastLoopTime = getTime();
  }

  public double getTime(){
    return System.nanoTime();
  }

  public float getElapsedTime() {
    double time = getTime();
    float elapsedTime = (float) (time - lastLoopTime);
    lastLoopTime = time;
    return elapsedTime;
  }

  public double getLastLoopTime(){
    return lastLoopTime;
  }

}
