public class GameEngine implements Runnable {
  // Frame rate and update caps
  public static final int TARGET_FPS = 75;
  public static final int TARGET_UPS = 30;

  private final Window window;

  private final Thread gameLoopThread;

  private final Timer timer;

  private final IGameLogic gameLogic;

  /*
  * Constructor
  * */
  public GameEngine(
          String windowTitle,
          int width,
          int height,
          boolean vSync,
          IGameLogic gameLogic
          ){
    gameLoopThread = new Thread(this, "GAME_LOOP_Thread");
    window = new Window(windowTitle, width, height, vSync);
    this.gameLogic = gameLogic;
    timer = new Timer();
  }

  public void start() {
    String osName = System.getProperty("os.name");
    if (osName.contains("Mac")) {
      gameLoopThread.run();
    } else {
      gameLoopThread.start();
    }
  }

  @Override
  public void run() {
    try {
      init();
      gameLoop();
    } catch (Exception excp) {
      excp.printStackTrace();
    }
  }

  protected void init() throws Exception {
    window.init();
    timer.init();
    gameLogic.init();
  }


  /*
  * This is a Fixed Step game loop
  * */
  protected void gameLoop() {
    float elapsedTime;
    float accumulator = 0;
    float interval = 1 / TARGET_UPS;

    // controls the exit condition of the loop
    boolean running = true;
    while (running && !window.windowShouldClose()){
      elapsedTime = timer.getElapsedTime();
      accumulator += elapsedTime;

      input();

      while (accumulator >= interval) {
        update(interval);
        accumulator -= interval;
      }

      render();

      if (!window.isvSync()) {
        sync();
      }
    }
  }

  private void sync() {
    float loopSlot = 1 / TARGET_FPS;
    double endTime = timer.getLastLoopTime() + loopSlot;

    while (timer.getTime() < endTime){
      try {
        Thread.sleep(1);
      } catch (InterruptedException ie) {
      }
    }
  }

  protected void input() {
    gameLogic.input(window);
  }

  protected void update(float interval) {
    gameLogic.update(interval);
  }

  protected void render() {
    gameLogic.render(window);
    window.update();
  }
} //end of GameEngine class
